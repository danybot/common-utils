package com.danybot.CommonUtils;

import java.util.HashMap;
import java.util.Map;

public class CacheManager {

	private static Map<String, Object> cacheMap = new HashMap<String, Object>();

	/**
	 * get the cache data by the key. If there is no key then it will return null.
	 * It is advisable to null check the return object
	 * 
	 * @param key  {String}
	 * @param type {Class<T>}
	 * @return {T}
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getCache(String key, Class<T> type) {
		if (type.isInstance(cacheMap.get(key)))
			return (T) cacheMap.get(key);
		return null;
	}

	/**
	 * add the cache data by the key, Note if the key is already present then it
	 * will update the data without any warning.
	 * 
	 * @param key {String}
	 * @param obj {Object}
	 */
	public static void addCache(String key, Object obj) {
		cacheMap.put(key, obj);
	}

	/**
	 * clear the cache for specified key only if the key is present.
	 * 
	 * @param key {String}
	 */
	public static void removeCache(String key) {
		if (cacheMap.containsKey(key))
			cacheMap.remove(key);
	}

	/**
	 * clear all the cache map.
	 */
	public static void clearCache() {
		cacheMap.clear();
	}
}
