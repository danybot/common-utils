package com.danybot.CommonUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileAccessor {

	private final static Configuration config = Configuration.getInstance();
	private final static String filePath = config.getProperty("storage.file.path", "resource/storage");
	private final static String fileExtension = config.getProperty("storage.file.extension", "bob");

	/**
	 * read the object file and cast it to the class type. It dumps all the
	 * exceptions and log the Corresponding error. If there is an error in accessing
	 * or reading the file then it will return null.
	 * 
	 * @param key  {String}
	 * @param type {Class}
	 * @return {T}
	 */
	@SuppressWarnings("unchecked")
	public static <T> T read(String key, Class<T> type) {
		return (T) FileAccessor.innerRead(key);
	}

	/**
	 * write the object to the file(named after the key value). Object should be
	 * Serializable to save and retrieve.
	 * 
	 * @param key    {String}
	 * @param object {Object}
	 * @return {boolean}
	 */
	public static boolean write(String key, Object object) {
		if (Objects.isNull(object)) {
			log.error("Could not save null [key : {}]", key);
			return false;
		} else if (object instanceof Serializable == false) {
			log.error("Class {} must implements Serializable.", object.getClass().getName());
			return false;
		} else {
			FileAccessor.innerWrite(key, object);
			return true;
		}
	}

	/**
	 * Read the file from the specified folder structure mentioned in the config (By
	 * default it will be from /resource/storage). It will mask all the exception
	 * and return null, if any exception happens.
	 * 
	 * @param fileName {String}
	 * @return {Object}
	 */
	private static Object innerRead(String fileName) {
		log.info("Accessing the File {}", fileName);
		File file = FileAccessor.getFile(fileName);
		ObjectInputStream objectInputStream = null;
		if (file.exists()) {
			try {
				objectInputStream = new ObjectInputStream(new FileInputStream(file));
				return objectInputStream.readObject();
			} catch (FileNotFoundException e) {
				log.error("File not found save the file before read");
			} catch (IOException e) {
				log.error("Error in accessing the File");
			} catch (ClassNotFoundException e) {
				log.error("Cannot map the Object");
			} finally {
				if (Objects.nonNull(objectInputStream))
					try {
						objectInputStream.close();
					} catch (IOException e) {
						log.warn("Unabel to close the ObjectInputStream");
					}
			}
		}
		return null;
	}

	/**
	 * write the object in file. Default path will be /resource/storage. However it
	 * can be override form config using storage.file.path property.
	 * 
	 * @param fileName {String}
	 * @param object   {Object}
	 * @return {boolean}
	 */
	private static boolean innerWrite(String fileName, Object object) {
		ObjectOutputStream objectOutputStream = null;
		boolean state = true;
		try {
			File file = FileAccessor.getFile(fileName);
			log.warn("Deleting the previous File {}", fileName);
			file.delete();
			log.info("Creating new file {}", fileName);
			file.createNewFile();

			log.info("Start writing the object in file");
			objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
			objectOutputStream.writeObject(object);
			log.info("File have been Updated {}", fileName);
		} catch (IOException e) {
			log.error("Error in creating or Accessing the File.");
			state = false;
		} finally {
			if (Objects.nonNull(objectOutputStream))
				try {
					objectOutputStream.close();
				} catch (IOException e) {
					log.error("Could not close the out stream properly. It might cause memory leak");
				}
		}
		return state;
	}

	/**
	 * Prepare the file with the specified folder path in config.
	 * 
	 * @param fileName {String}
	 * @return {File}
	 */
	public static File getFile(String fileName) {
		SystemUtils.makeDirectories(filePath + "/");
		String relativeFilePath = filePath + "/" + fileName + (fileExtension.isEmpty() ? "" : "." + fileExtension);
		log.info("Relative file Path {}", relativeFilePath);
		return new File(relativeFilePath);
	}
}
