package com.danybot.CommonUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.danybot.CommonUtils.Constant.StringLiterals;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class YamlConfiguration {

	private static final YamlParser yamlParser = new YamlParser();

	private static final Map<String, YamlConfiguration> yamlConfigurationMap = new LinkedHashMap<String, YamlConfiguration>();

	private final Map<String, String> configRawModelMap;

	/**
	 * private constructor to mask instantiating outside. Always use
	 * <code>getInstance</code> method to create or get the instance.
	 * 
	 * @param yamlConfigurationFile {File}
	 */
	private YamlConfiguration(File yamlConfigurationFile) {
		this.configRawModelMap = new LinkedHashMap<String, String>();
		this.loadRawModel(yamlConfigurationFile);
	}

	/**
	 * read the file and parse into separate model with the key.
	 * 
	 * @param {File}
	 */
	private void loadRawModel(File yamlConfigurationFile) {
		log.info("Parsing file {}", yamlConfigurationFile.getName());
		BufferedReader bufferedReader = null;
		try {
			int indentSize = 0;
			String buffer, modelKey = null;
			StringBuilder modelBuilder = new StringBuilder();
			bufferedReader = new BufferedReader(new FileReader(yamlConfigurationFile));
			while ((buffer = bufferedReader.readLine()) != null) {
				if (buffer.isEmpty()) {
					continue;
				} else if (Character.isWhitespace(buffer.charAt(0))) {
					if (indentSize == 0) {
						indentSize = buffer.indexOf(buffer.trim());
					}
					modelBuilder.append(buffer.substring(indentSize)).append(StringLiterals.NEW_LINE);
				} else {
					if (modelBuilder.length() > 0) {
						this.configRawModelMap.put(modelKey, modelBuilder.toString());
					}
					modelKey = buffer.substring(0, buffer.lastIndexOf(":")).trim();
					modelBuilder = new StringBuilder();
					indentSize = 0;
					log.info(StringFormatter.indent("Pasing Model {}", 1), modelKey);
				}
			}
			if (modelBuilder.length() > 0) {
				this.configRawModelMap.put(modelKey, modelBuilder.toString());
			}
		} catch (IOException e) {
			log.error("Could not read the file");
		} finally {
			if (Objects.nonNull(bufferedReader)) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					log.warn("Unable to close the reader");
				}
			}
		}
	}

	/**
	 * get all the model key. If there is not file or empty file then it will return
	 * an empty list.
	 * 
	 * @return {List<String}
	 */
	public List<String> getAllKey() {
		return new ArrayList<>(this.configRawModelMap.keySet());
	}

	/**
	 * get model parse the rawString and cast it to the class type. Here the
	 * snakeYaml loads the string. If there is no key then it will return null.
	 * 
	 * @param key  {String}
	 * @param type {Class<T>}
	 */
	public <T> T getModel(String key, Class<T> type) {
		if (this.configRawModelMap.containsKey(key)) {
			return yamlParser.parse(this.configRawModelMap.get(key), type);
		}
		return null;
	}

	/**
	 * It loads the yaml configuration file if it finds the new file. It Instance
	 * the File by given path and access the yaml configuration.
	 * 
	 * @param filePath {String}
	 * @return {YamlConfiguration}
	 */
	public static YamlConfiguration getInstance(String filePath) {
		File yamlConfigurationFile = new File(filePath);
		return YamlConfiguration.getInstance(yamlConfigurationFile);
	}

	/**
	 * loads the yaml configuration from the file. If the file is an folder or not
	 * exist then it will return as null. we need to make sure the null check or
	 * verify the file before getting instance.
	 * 
	 * @param yamlConfigurationFile {File}
	 * @return {YamlConfiguration}
	 */
	public static YamlConfiguration getInstance(File yamlConfigurationFile) {
		String absolutePath = yamlConfigurationFile.getAbsolutePath();
		if (!yamlConfigurationMap.containsKey(absolutePath)) {
			if (yamlConfigurationFile.exists() && yamlConfigurationFile.isFile()) {
				yamlConfigurationMap.put(absolutePath, new YamlConfiguration(yamlConfigurationFile));
			} else {
				log.error("{} is not an valid file path", yamlConfigurationFile.getName());
				return null;
			}
		}
		return yamlConfigurationMap.get(absolutePath);
	}
}
