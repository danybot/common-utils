package com.danybot.CommonUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.danybot.CommonUtils.Callback.LoggerCallBack;

@Deprecated
public class Logger {

	private final static Configuration config = Configuration.getInstance();
	private final static String timeFormat = config.getProperty("log.timeformat", "HH:mm:ss:SSS");
	private final static String logPattern = config.getProperty("log.pattern", "%s %s %s");
	private final static boolean disableLog = Boolean.valueOf(config.getProperty("log.disable", "false"));

	private static LoggerCallBack loggerCallBack = message -> System.out.println(message);

	/**
	 * private constructor to prevent the initialization of the logger.
	 */
	private Logger() {
	}

	/**
	 * log the information with the [Info] tag. It can also configured to print the
	 * date and time by using the log.timeformat property in config file.
	 * 
	 * @param info {String}
	 */
	public static void info(String info, Object... args) {
		log(String.format(logPattern, getTimeStamp(), LOG.INFO.value(), String.format(info, args)));
	}

	/**
	 * log the error with the [Error] tag with along with the date and time. It can
	 * be formatted using the log.timeformat property in config file.
	 * 
	 * @param error {String}
	 */
	public static void error(String error, Object... args) {
		log(String.format(logPattern, getTimeStamp(), LOG.ERROR.value(), String.format(error, args)));
	}

	/**
	 * log the warning with the [Warn] tag. It can also configured to print the date
	 * and time by using the log.timeformat property in config file.
	 * 
	 * @param warn {String}
	 */
	public static void warn(String warn, Object... args) {
		log(String.format(logPattern, getTimeStamp(), LOG.WARN.value(), String.format(warn, args)));
	}

	/**
	 * main log method to console the message. it can be masked by setting the
	 * disableLog flag using the log.disable property.
	 * 
	 * @param message {String}
	 */
	private static void log(String message) {
		if (!disableLog)
			loggerCallBack.onLog(message);
		// TODO Need Implementation of File logging.
	}

	public static void setLoggerCallBack(LoggerCallBack loggerCallBack) {
		Logger.loggerCallBack = loggerCallBack;
	}

	/**
	 * return the date time stamp based on tiimeFormat pattern. By default it will
	 * format only the hour:minute:second:milliSecond. It can be customized by using
	 * the log.timeFormate
	 * 
	 * @return {String}
	 */
	private static String getTimeStamp() {
		SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
		return sdf.format(new Date());
	}

	public enum LOG {

		INFO("INFO"), WARN("WARN"), ERROR("ERROR");

		private String log;

		/**
		 * constructor initialize the value by aligning it in left.
		 * 
		 * @param log {String}
		 */
		LOG(String log) {
			this.log = StringFormatter.alignLeft(log, 5);
		}

		/**
		 * get the value of each level of log
		 * 
		 * @return {String}
		 */
		public String value() {
			return this.log;
		}
	}
}
