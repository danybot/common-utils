package com.danybot.CommonUtils;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

public final class SystemUtils {

	public enum OsType {
		Windows, MacOs, Linux, Other
	};

	private static OsType os;

	private static String lineSeparator = System.getProperty("line.separator");

	public static OsType getOsType() {
		if (Objects.isNull(os)) {
			String Os = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
			if (Os.contains("mac") || Os.contains("darwin")) {
				os = OsType.MacOs;
			} else if (Os.contains("win")) {
				os = OsType.Windows;
			} else if (Os.contains("nux")) {
				os = OsType.Linux;
			} else {
				os = OsType.Other;
			}
		}
		return os;
	}

	/**
	 * handy wrapper for pausing the program. It handles the exceptions so if there
	 * is any error on pausing the program then it will continue the main logic
	 * without terminating the main program.
	 * 
	 * @param milliSecond {long}
	 */
	public static void waitForMilliSecond(long milliSecond) {
		try {
			Thread.sleep(milliSecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * make the directories if it does not exit. Note Only folder path need to be
	 * mentioned, If it and file type then directories wont be created.
	 * 
	 * @param relativeFilePath {String}
	 */
	public static void makeDirectories(String relativeFilePath) {
		File file = new File(relativeFilePath);
		if (!file.isFile()) {
			file.mkdirs();
		}
	}

	public static String getLineSeperator() {
		return lineSeparator;
	}
}
