package com.danybot.CommonUtils;

public class StringFormatter {

	private static final String EMPTY_SPACE = " ";

	/**
	 * Align the string to the left. It will also append the space to match the
	 * size. If the String exceed the length the ellipsis will be created.
	 * 
	 * @param str  {String}
	 * @param size {Integer}
	 * @return {String}
	 */
	public static String alignLeft(String str, int size) {
		int remLength = size - str.length();
		if (remLength < 0)
			return setEllipsis(str, size);
		return remLength == 0 ? str : str + buildString(EMPTY_SPACE, remLength);
	}

	/**
	 * Align the string to the right by appending space before the string. If the
	 * String exceed the length then ellipsis will be created.
	 * 
	 * @param str  {String}
	 * @param size {Integer}
	 * @return {String}
	 */
	public static String alignRight(String str, int size) {
		int remLength = size - str.length();
		if (remLength < 0)
			return setEllipsis(str, remLength);
		return remLength == 0 ? str : buildString(EMPTY_SPACE, remLength) + str;
	}

	/**
	 * Align the string center by appending the empty space before and after the
	 * string. If the string exceed the size then ellipsis will be created.
	 * 
	 * @param str  {String}
	 * @param size {Integer}
	 * @return {String}
	 */
	public static String alignCenter(String str, int size) {
		int remLength = size - str.length();
		if (remLength < 0)
			return setEllipsis(str, remLength);
		return remLength == 0 ? str
				: buildString(EMPTY_SPACE, remLength / 2) + str
						+ buildString(EMPTY_SPACE, remLength - (remLength / 2));
	}

	/**
	 * set the ellipsis for the string only if the string length exceed the size. It
	 * will append (...) at the end of sub string.
	 * 
	 * @param str   {String}
	 * @param limit {Integer}
	 * @return {String}
	 */
	public static String setEllipsis(String str, int limit) {
		if (str.length() > limit)
			return str.substring(0, limit - 3) + "...";
		return str;
	}

	/**
	 * generate the string by repeating the pattern.
	 * 
	 * @param pattern {String}
	 * @param size    {Integer}
	 * @return {String}
	 */
	public static String buildString(String pattern, int size) {
		return String.format("%" + size + "s", "").replace(EMPTY_SPACE, pattern);
	}

	/**
	 * indent the string, By default Indent size will be 4 space.
	 */
	public static String indent(String str, int step) {
		return buildString(EMPTY_SPACE, step * 4) + str;
	}
}
