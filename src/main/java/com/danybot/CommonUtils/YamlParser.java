package com.danybot.CommonUtils;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.BaseConstructor;

public class YamlParser {

	private final Yaml yaml;

	/**
	 * Constructor without a base constructor for yaml
	 */
	public YamlParser() {
		yaml = new Yaml();
	}

	/**
	 * Constructor with base constructor for yaml, which tends to call the call on
	 * instantiating the object. It might be handy to use in parsing, if all the
	 * entities in yaml file is same type.
	 * 
	 * @param constructor
	 */
	public YamlParser(BaseConstructor constructor) {
		yaml = new Yaml(constructor);
	}

	/**
	 * parse the string and cast to the class type. It might be handy for loading
	 * the model type yaml.
	 * 
	 * @param yamlString {String}
	 * @param type       {Class<T>}
	 * @return {<T> T}
	 */
	public <T> T parse(String yamlString, Class<T> type) {
		return yaml.loadAs(yamlString, type);
	}

	/**
	 * pare the string and return the Object type. It helps to handle or parse the
	 * non model type yaml string.
	 * 
	 * @param yamlString {String}
	 * @return {<T> T}
	 */
	public <T> T parse(String yamlString) {
		return yaml.load(yamlString);
	}
}
