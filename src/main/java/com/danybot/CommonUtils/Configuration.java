package com.danybot.CommonUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Configuration extends Properties {

	private static final long serialVersionUID = -3415920742235139943L;
	private static Configuration configuration;
	private static final String FILE_PATH = "config.properties";

	/**
	 * private constructor to mask the multiple instance and make it an singleTon
	 * class. It load the default location of config.application file.
	 */
	private Configuration() {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(FILE_PATH);
			load(fileReader);
		} catch (FileNotFoundException e) {
			log.error("Could not load config file");
		} catch (IOException e) {
			log.error("Error in config file");
		} finally {
			try {
				if (Objects.nonNull(fileReader))
					fileReader.close();
			} catch (IOException e) {
				log.error("Could not close the config file");
			}
		}
	}

	/**
	 * get instance for the singelTon class.
	 * 
	 * @return {Configuration}
	 */
	public static Configuration getInstance() {
		if (configuration == null)
			configuration = new Configuration();
		return configuration;
	}
}
